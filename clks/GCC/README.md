If you are going to do this part, don't. It's painful.

# GCC
## Structure of a driver
* copyright header
* `#include`s
* `enum` with all possible values that are encountered within `freq_tbl`s and `parent_map`s
* `parent_map`s and `const char *`s representations of them
* clk structs which describe clocks (see Structs.md file for more information)
* `static struct clk_regmap *gcc_msmXXXX_clocks[]`, which describes all clocks in `[CLOCK_FROM_DT_BINDINGS] == &corresponding clock generated from structs` format
* `static struct gdsc *gcc_msmXXXX_gdscs[]`, which describes all gdscs in `[[GDSC_FROM_DT_BINDINGS] == &corresponding gdsc generated from struct` format
* `static const struct qcom_reset_map gcc_msmXXXX_resets[]`, which sets all reset values in `[RESET_FROM_DT_BINDINGS] == { value for reset in hexadecimal format }` format
* `static const struct regmap_config gcc_msmXXXX_regmap_config`, which is something (TODO: find out of that is)
* `static const struct qcom_cc_desc gcc_msmXXXX_desc`, which is something that I need to document (TODO: document this struct)
* `static const struct of_device_id gcc_msmXXXX_match_table[]`, which contains compatible for use in device trees
* `static int gcc_msmXXXX_probe(struct platform_device *pdev)`, which sets frequencies for some clocks
* `static struct platform_driver gcc_msmXXXX_driver` (TODO: document this)
* `static int __init gcc_msmXXXX_init(void)` and `static void __exit gcc_msmXXXX_exit(void)`, which can be copied from other drivers
* `MODULE_DESCRIPTION;`, `MODULE_LICENSE` and `MODULE_ALIAS`, pretty self-explanatory ;)
