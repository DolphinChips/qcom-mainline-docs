# Structs

## Structs equivalents in downstream and mainline 
* (mainline struct == downstream struct)
* `clk_regmap` and `clk_pll` == `pll_vote_clk`
* something for a??-pll == `pll_clk`
* `clk_rcg2` == `rcg_clk`
* `clk_branch` == `branch_clk`
* `freq_tbl` == `clk_freq_tbl`
* (not supported, used for debug clks) == `clk_mux_ops` == `mux_clk`
* (not supported, used for debug clks) == `clk_ops`
* (not found in mainline?) == `gate_clk`
* `clk_branch` == `local_vote_clk`
* (not found in mainline) == `measure_clk`
* (not supported, debug_clk?) == `measure_clk_data`
* `pll_freq_tbl` (in a53-pll.c) == `pll_freq_tbl`
* `gdsc` == `qcom,gdsc`

## Comparison of structs

### `clk_regmap`&`clk_pll` and `pll_vote_clk`
#### Downstream:
```c
static struct pll_vote_clk gpll0_clk_src = {
	.en_reg = (void __iomem *)APCS_GPLL_ENA_VOTE,
	.en_mask = BIT(0),
	.status_reg = (void __iomem *)GPLL0_STATUS,
	.status_mask = BIT(17),
	.soft_vote = &soft_vote_gpll0,
	.soft_vote_mask = PLL_SOFT_VOTE_PRIMARY,
	.base = &virt_bases[GCC_BASE],
	.c = {
		.parent = &xo_clk_src.c,
		.rate = 800000000,
		.dbg_name = "gpll0_clk_src",
		.ops = &clk_ops_pll_acpu_vote,
		CLK_INIT(gpll0_clk_src.c),
	},
};
```
#### Mainline:
```c
static struct clk_pll gpll0 = {
	.l_reg = 0x21004,
	.m_reg = 0x21008,
	.n_reg = 0x2100c,
	.config_reg = 0x21014,
	.mode_reg = 0x21000,
	.status_reg = 0x2101c,
	.status_bit = 17,
	.clkr.hw.init = &(struct clk_init_data){
		.name = "gpll0",
		.parent_names = (const char *[]){ "xo" },
		.num_parents = 1,
		.ops = &clk_pll_ops,
	},
};

static struct clk_regmap gpll0_vote = {
	.enable_reg = 0x45000,
	.enable_mask = BIT(0),
	.hw.init = &(struct clk_init_data){
		.name = "gpll0_vote",
		.parent_names = (const char *[]){ "gpll0" },
		.num_parents = 1,
		.ops = &clk_pll_vote_ops,
	},
};
```

#### Notes:
* regmap == `static struct clk_regmap gpll0_vote`; pll == `static struct clk_pll gpll0`; DS == downstream
* only structs above were compared, further investigation might be needed
* `.en_reg` in DS and `enable_reg` in regmap have the same values 
(see https://github.com/msm8916-mainline/android_kernel_qcom_msm8916/blob/master/drivers/clk/qcom/clock-gcc-8916.c#L45-L233 for values in DS)
* `.en_mask` in DS and `.enable_mask` in regmap have the same values
* `.status_reg` in DS and `.status_reg` in pll are the same
* `.status_mask` in DS and `.status_bit` in pll have the same values 
(eхcept DS has `BIT(17)` and mainline has just `17`)
* `PLL_SOFT_VOTE_PRIMARY` is defined in include/soc/qcom/clock-pll.h at L99 and doesn't seem to have equivalent in mainline
* `.base` doesn't seem to be used in mainline
* `.c.parent` actually matches one in `static struct clk_pll gpll0`, vote clocks in regmap seem to be children of their non-vote equivalents 
(eg. parent of `gpll0_vote` is `gpll0`)
* `.c.rate` doesn't seem to be used in mainline either
* `.c.dbg_name` in DS might have the same meaning as `.c.name` in mainline
* `.c.ops` seems to be `.clkr.hw.init.ops = &clk_pll_ops` for pll and `.hw.init.ops = &clk_pll_vote_ops` for regmap
* `.l_reg` in pll is `GPLL0_L_VAL` in DS
* `.m_reg` in pll is `GPLL0_M_VAL` in DS
* `.n_reg` in pll is `GPLL0_N_VAL` in DS
* `.config_reg` in pll is `GPLL0_CONFIG_CTL` in DS
* `.mode_reg` in pll is `GPLL0_MODE` in DS

### `clk_rcg2` and `rcg_clk`
#### Downstream:
```c
static struct rcg_clk apss_ahb_clk_src = {
	.cmd_rcgr_reg = APSS_AHB_CMD_RCGR,
	.set_rate = set_rate_hid,
	.freq_tbl = ftbl_apss_ahb_clk,
	.current_freq = &rcg_dummy_freq,
	.base = &virt_bases[GCC_BASE],
	.c = {
		.dbg_name = "apss_ahb_clk_src",
		.ops = &clk_ops_rcg,
		CLK_INIT(apss_ahb_clk_src.c),
	},
};
```
#### Mainline:
```c
static struct clk_rcg2 apss_ahb_clk_src = {
	.cmd_rcgr = 0x46000,
	.hid_width = 5,
	.parent_map = gcc_xo_gpll0_map,
	.freq_tbl = ftbl_apss_ahb_clk,
	.clkr.hw.init = &(struct c	_init_data){
		.name = "apss_ahb_clk_src",
		.parent_names = gcc_xo_gpll0,
		.num_parents = 2,
		.ops = &clk_rcg2_ops,
	},
};
```

#### Notes:
* more structs need to be compared
* `.cmd_rcgr_reg` and `.cmd_rcgr` have the same values
(see https://github.com/msm8916-mainline/android_kernel_qcom_msm8916/blob/master/drivers/clk/qcom/clock-gcc-8916.c#L45-L233 for values in downstream)
* `.set_rate` doesn't seem to be used on mainline
* `.freq_tbl` and `.freq_tbl` are the same, duh
* `.current_freq` doesn't seem to be used in mainline
* `.base` doesn't seem to be used in mainline
* `.hid_width` is "number of bits in half integer divider". I wish I knew what "half integer divider" means. Anyway, they all are 5 in gcc-msm8916.c.
* `.parent_map` is black magic that I don't understand
* `.clkr.hw.init.parent_names` is like `.parent_map`, but array of strings
* `.clkr.hw.init.num_parents` is `sizeof(.parent_names)`
* `.clkr.hw.init.ops` always should be `&clk_rcg2_ops` I guess

### `clk_branch` and `branch_clk`
#### Downstream:
```c
static struct branch_clk gcc_blsp1_qup1_i2c_apps_clk = {
	.cbcr_reg = BLSP1_QUP1_I2C_APPS_CBCR,
	.has_sibling = 0,
	.base = &virt_bases[GCC_BASE],
	.c = {
		.dbg_name = "gcc_blsp1_qup1_i2c_apps_clk",
		.parent = &blsp1_qup1_i2c_apps_clk_src.c,
		.ops = &clk_ops_branch,
		CLK_INIT(gcc_blsp1_qup1_i2c_apps_clk.c),
	},
};
```
#### Mainline:
```c
static struct clk_branch gcc_blsp1_qup1_i2c_apps_clk = {
	.halt_reg = 0x02008,
	.clkr = {
		.enable_reg = 0x02008,
		.enable_mask = BIT(0),
		.hw.init = &(struct clk_init_data){
			.name = "gcc_blsp1_qup1_i2c_apps_clk",
			.parent_names = (const char *[]){
				"blsp1_qup1_i2c_apps_clk_src",
			},
			.num_parents = 1,
			.flags = CLK_SET_RATE_PARENT,
			.ops = &clk_branch2_ops,
		},
	},
};
```
#### Notes:
* notes here might not apply to `local_vote_clk`!
* `.cbcr_reg`, `.clkr.enable_reg` and `.halt_reg` have the same values
(see https://github.com/msm8916-mainline/android_kernel_qcom_msm8916/blob/master/drivers/clk/qcom/clock-gcc-8916.c#L45-L233 for values in downstream)
* `.has_sibling` doesn't seem being used on mainline(?)
* `.base` doesn't seem being used on mainline
* `.c.dbg_name` and `.hw.init.name` have the same values
* `.clkr.enable_mask` seems to always be `BIT(0)` (but this does not apply to `local_vote_clk` clks)
* all `branch_clk` seem to be parent of theirselves (unless it's `local_vote_clk` in downstream)
* `.clkr.hw.init.num_parents` is `sizeof(.parent_names)`, which might always be 1
* `.clkr.hw.init.flags` seem to always be `CLK_SET_RATE_PARENT` (unless it's `local_vote_clk` in downstream)
* `.clkr.hw.init.ops` seem to always be `&clk_branch2_ops`

### `freq_tbl` and `clk_freq_tbl`
#### Downstream:
```c
static struct clk_freq_tbl ftbl_apss_ahb_clk[] = {
	F(  19200000,	    xo_a,   1,	  0,	0),
	F(  50000000,	   gpll0,  16,	  0,	0),
	F(  100000000,	   gpll0,   8,	  0,	0),
	F(  133330000,	   gpll0,   6,	  0,	0),
	F_END
};
```
#### Mainline:
```c
static const struct freq_tbl ftbl_apss_ahb_clk[] = {
	F(19200000, P_XO, 1, 0, 0),
	F(50000000, P_GPLL0, 16, 0, 0),
	F(100000000, P_GPLL0, 8, 0, 0),
	F(133330000, P_GPLL0, 6, 0, 0),
	{ }
};
```
#### Notes:
* they are basically the same but with different indentation and `s/F_END/{ }/g`
* second argument in `F(f, s, h, m, n)` macro comes from [there](https://github.com/msm8916-mainline/android_kernel_qcom_msm8916/blob/master/drivers/clk/qcom/clock-gcc-8916.c#L236-L242) in downstream
and from [there](https://github.com/torvalds/linux/blob/master/drivers/clk/qcom/gcc-msm8916.c#L28-L43) in mainline

### `clk_branch` and `local_vote_clk`
#### Downstream:
```c
static struct local_vote_clk gcc_blsp1_ahb_clk = {
	.cbcr_reg = BLSP1_AHB_CBCR,
	.vote_reg = APCS_CLOCK_BRANCH_ENA_VOTE,
	.en_mask = BIT(10),
	.base = &virt_bases[GCC_BASE],
	.c = {
		.dbg_name = "gcc_blsp1_ahb_clk",
		.ops = &clk_ops_vote,
		CLK_INIT(gcc_blsp1_ahb_clk.c),
	},
};
```
#### Mainline:
```c
static struct clk_branch gcc_blsp1_ahb_clk = {
	.halt_reg = 0x01008,
	.halt_check = BRANCH_HALT_VOTED,
	.clkr = {
		.enable_reg = 0x45004,
		.enable_mask = BIT(10),
		.hw.init = &(struct clk_init_data){
			.name = "gcc_blsp1_ahb_clk",
			.parent_names = (const char *[]){
				"pcnoc_bfdcd_clk_src",
			},
			.num_parents = 1,
			.ops = &clk_branch2_ops,
		},
	},
};
```
#### Notes:
* `.cbcr_reg` and `.halt_reg` have the same values
(see https://github.com/msm8916-mainline/android_kernel_qcom_msm8916/blob/master/drivers/clk/qcom/clock-gcc-8916.c#L45-L233 for values in downstream)
* `.vote_reg` and `.clkr.enable_reg` have the same values
* `.en_mask` and `.clkr.enable_mask` have the same values
* `.base` doesn't seem to be used in mainline
* `.c.dbg_name` and `.clkr.hw.init.name` have the same values
* `.halt_check` seems to always be `BRANCH_HALT_VOTED`
* `.clkr.hw.init.parent_names` is just like `.parent_map`, something that I don't understand ;-;
* `.clkr.hw.init.num_parents` is `sizeof(.parent_names)`
* `.clkr.hw.init.ops` seems to always be `&clk_branch2_ops`
* some clks have `.clkr.hw.init.flags` which is always set to `CLK_SET_RATE_PARENT`. I have no idea where this comes from, in downstream they don't look anything like special

### `gdsc` and `qcom,gdsc`
#### Downstream:
```c
gdsc_venus: qcom,gdsc@184c018 {
		compatible = "qcom,gdsc";
		regulator-name = "gdsc_venus";
		reg = <0x184c018 0x4>;
		status = "disabled";
	};
```
#### Mainline:
```c
static struct gdsc venus_gdsc = {
	.gdscr = 0x4c018,
	.pd = {
		.name = "venus",
	},
	.pwrsts = PWRSTS_OFF_ON,
};
```
#### Notes:
* `qcom,gdsc` actually comes from device tree in downstream
* `.gdscr` takes its value from last 5 bytes of `qcom,gdsc` address (eg. @18**4c018** is `.gdscr = 0x4c018`)
* `.pd.name` comes from name of gdsc without `_gdsc`
* `.pwrsts` seems to always be `PWRSTS_OFF_ON`

## Other structs

### `parent_map`
`parent_map` contains all possible parents for a clock. Downstream doesn't have this struct, instead all possible parents located in `clk_freq_tbl` and use values from [here](https://github.com/msm8916-mainline/android_kernel_qcom_msm8916/blob/master/drivers/clk/qcom/clock-gcc-8916.c#L236-L242).

Typical `parent_map` looks like this:
```c
static const struct parent_map gcc_xo_gpll0_map[] = {
	{ P_XO, 0 },
	{ P_GPLL0, 1 },
};

static const char * const gcc_xo_gpll0[] = {
	"xo",
	"gpll0_vote",
};
```
* `parent_map` contains all possible parents for clock, those are used to get a certain frequency 
* first argument in `{ P_XO, 0 }` is defined in `enum` several lines above, second argument comes from L236-L242 in downstream driver for clock-gcc-8916
* `parent_map` can be generated from downstream `clk_freq_tbl`. Copy all `clk_freq_tbl` to blank file, add `#define F(f, s, div, m, n) { s, s##_source_val }` and 
values from [here](https://github.com/msm8916-mainline/android_kernel_qcom_msm8916/blob/master/drivers/clk/qcom/clock-gcc-8916.c#L236-L242) on top, run `gcc -E` on your file, 
rename and remove duplicates (thanks to https://github.com/Junak for the tip!)
* strings in `static const char *` seem to come from `.clkr.hw.init.name` (?)

### `regmap_config`
`regmap_config` is common struct for all clock drivers in Linux and has far more properties than ones in Qualcomm mainline clock drivers.

Typical `regmap_config` looks like this:
```c
static const struct regmap_config gcc_msm8916_regmap_config = {
	.reg_bits	= 32,
	.reg_stride	= 4,
	.val_bits	= 32,
	.max_register	= 0x80000,
	.fast_io	= true,
};
```
* `.max_register` comes from [`reg` property in `gcc` node in downstream dts](https://github.com/msm8916-mainline/android_kernel_qcom_msm8916/blob/master/arch/arm/boot/dts/qcom/msm8916.dtsi#L278)
* everything else is the same in all drivers
